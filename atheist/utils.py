# -*- mode: python; coding: utf-8 -*-

from __future__ import with_statement
import os, sys
import time
import string
import subprocess
import types
import logging
import threading
import ConfigParser
import logging
import abc


from pyarco.Conio import *

from atheist.const import *

import atheist.log
import atheist.gvar
Log = atheist.gvar.Log


def count(n, unit):
    """
    >>> count(3, 'tick')
    '3 ticks'
    >>> count(1, 'dog')
    '1 dog'
    """
    return "%s %s%s" % (n, unit, 's' if n>1 else '')

def run_cmd(cmd):
    devnull = open('/dev/null', 'w')
    ps = subprocess.Popen(cmd, shell=True,
                          stdout=subprocess.PIPE, stderr=devnull)
    output = ps.communicate()[0]
    devnull.close()
    ps.stdout.close()
    return ps.returncode, output


def remove_dups(val):
    """
    >>> remove_dups([1,2,3,1,2])
    [1, 2, 3]
    >>> remove_dups([1,1,2,1,2,1])
    [1, 2]
    """
    retval = []
    for x in val:
        if x not in retval: retval.append(x)
    return retval



def relpath(path=None):
    if path is None:
        path = relpath.root

    if not relpath.enable:
        return path

    try:
        retval = os.path.relpath(path, relpath.root)
    except ValueError:
        return path

    if not retval[0] in [os.sep, os.curdir]:
        retval = os.curdir + os.sep + retval

    if len(retval) > len(path):
        return path

    return retval


def abspath(path, base=None):
    """Return an absolute path."""
    if os.path.isabs(path):
        return path

    if base is None:
        if isinstance(path, unicode):
            base = os.getcwdu()
        else:
            base = os.getcwd()

    path = os.path.join(base, path)
    return os.path.normpath(path)



def print_exc(exc, logger=logging):
    for line in exc.split('\n'):
        if line.strip(): logger.error('| ' + line)


class TreeRender:
    '''generic tree representation'''

    __metaclass__ = abc.ABCMeta

    @classmethod
    def draw(cls, container, callback, connector='', args=[]):
        retval = ''
        lon = len(container)
        for i,item in enumerate(container):
            brother = cls.norm_brother
            parent  = cls.norm_parent

            if lon == i+1:
                brother = cls.last_brother
                parent  = cls.last_parent

            retval += callback(item,
                               connector + parent,
                               connector + brother, *args)

        return retval


class UTF_TreeRender(TreeRender):
    norm_brother = u'├─'
    last_brother = u'└─'
    norm_parent = u'│  '
    last_parent =  '   '

class ASCII_TreeRender(TreeRender):
    norm_brother = '+-'
    last_brother = '`-'
    norm_parent = '|  '
    last_parent = '   '




def tree_draw(container, cb, conn='', args=[]):
    '''utility for arbitrary tree representation'''

    lon = len(container)
    for i,item in enumerate(container):
        brother = u'├─'
        parent  = u'│  '

        if lon == i+1:
            brother = u'└─'
            parent  = '   '

        cb(item,
           conn + parent,
           conn + brother, *args)


def process_exists(pid):
    return os.system("ps ef %s > /dev/null" % pid) == 0



def high(val):
#    """
#    >>> high('foo')
#    '\\x1b[1mfoo\\x1b[m'
#    """
    return cout(HIGH, str(val), NORM)


def remove_escape(args):
    return [x for x in args if x[0] != ESC]


def cout(*args):
#    print atheist.gvar.plain

    if atheist.gvar.plain:
        args = remove_escape(args)

    return "%s" % str.join('', args)


def cout_config(plain=None):
    if plain is not None:
        atheist.gvar.plain = plain
        return

    term = os.getenv('TERM')
    cterm = os.getenv('COLORTERM')
    atheist.gvar.plain = not (cterm != '' or 'color' in term or 'xterm' == term)
    Log.debug("UI: Color out set to: %s" % atheist.gvar.plain)
