# -*- mode: python; coding: utf-8 -*-

# atheist
#
# Copyright (C) 2009,2010,2011 David Villa Alises
#
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import os, sys, string, stat

import glob
import subprocess as subp
import logging
import time
import select
import signal
import copy
import exceptions, traceback
import socket
import StringIO
from functools import partial
import optparse
import types
import codecs
import commands
from itertools import chain
from operator import attrgetter


from pyarco.Type import Record, SortedDict, merge_uniq, accept, attributes, check_type
from pyarco.Pattern import Singleton
from pyarco.Thread import ThreadFunc
from pyarco.UI import ellipsis, ProgressBar
from pyarco.Conio import *
from pyarco.Thread import SimpleThreadPool

from atheist.const import *
from atheist.utils import *
import atheist.log
from atheist import gvar
Log = gvar.Log

from atheist.manager import Manager

if gvar.the_config is None:
    gvar.the_config = atheist.manager.Config()

the_config = gvar.the_config


class ConfigureError(Exception): pass

def assure_tmp_dir():
    if not os.path.exists(atheist.const.ATHEIST_TMP):
        os.makedirs(atheist.const.ATHEIST_TMP)
        return

#    for fname in os.listdir(atheist.const.ATHEIST_TMP):
#        os.remove(os.path.join(atheist.const.ATHEIST_TMP, fname))



def pretty(value):
    RESULT_STR = {\
        FAIL:      [LIGHT_RED,        STR_STATUS[FAIL]],
        OK:        [GREEN,            STR_STATUS[OK]],
        NOEXEC:    [GREY,             STR_STATUS[NOEXEC]],
        ERROR:     [BOLD , LIGHT_RED, STR_STATUS[ERROR]],
        UNKNOWN:   [GREY,             STR_STATUS[UNKNOWN]],
        TODO:      [PURPLE,           STR_STATUS[TODO]],
        SOFT_FAIL: [RED,              STR_STATUS[SOFT_FAIL]],
        MISCONF:   [PURPLE,          STR_STATUS[MISCONF]]
        }

    assert isinstance(value, int)
    return "[%s]" % cout(*RESULT_STR[value]+[NORM])


def file_template():
    # FIXME: Construir a partir de 'task_attrs'
    return '''# -*- mode:python; coding:utf-8 -*-

# Test(cmd,
#      cwd         = '.',
#      delay       = 0,
#      desc        = 'test template',
#      detach      = False,
#      env         = {},
#      expected    = 0,
#      must_fail   = False,
#      path        = '.',
#      template    = [],
#      timeout     = 5,
#      save_stderr = False,
#      save_stdout = False,
#      signal      = signal.SIGKILL)
'''

class task_mode:
    MAIN, \
    SETUP, TEARDOWN, \
    DIR_SETUP, DIR_TEARDOWN = ['-', 's', 't', 'S', 'T']


class ExecutionError(Exception):
    def __str__(self):
        return '<{0}>'.format(self.__class__.__name__)


def exec_file(fname, _global=None, _local=None, template={}):

    def error():
        print_exc(traceback.format_exc(), Log)
        Log.error("Errors in the testcase definition '%s'" % fname)
        raise ExecutionError()

    try:
        with file(fname) as fd:
            script = string.Template(fd.read()).safe_substitute(template)

        code = compile(script, fname, 'exec')
        exec(code) in _global, _local  # EXEC TEST FILE

    except IOError, e:
#        if e.errno == 2:
#            Log.warning("No such file or directory: '%s'" % fname)
#            return

        error()

    except Exception, e:
        error()


class Public:
    "Base class for classes available for the client tasks"


class Plugin:
    "Base class for plugins"

    @classmethod
    def get_options(cls, config, parser):
        return []

    @classmethod
    def config(cls, config, parser):
        pass

    @classmethod
    def is_enabled(cls):
        return True


class Reporter:
    def __init__(self, dst, handler=None):
        self.dst = dst
        self.width = 80
        assert isinstance(dst, str)
        self.logger = logging.getLogger(dst)
        self.logger.propagate = 0
        self.logger.setLevel(logging.INFO)

        if handler is None:
            handler = logging.StreamHandler()

        self.logger.addHandler(handler)

        if the_config.plain:
            self.tree = ASCII_TreeRender
        else:
            self.tree = UTF_TreeRender


    def add_case(self, case):
        "case by case basis reporting (usefull for non persistent files)"


    def do_render(self, suite):
        Log.debug("%s: sending report to '%s'" % (self.__class__.__name__, self.dst))
        report = self.render(suite)
        if report:
            self.logger.info(report)

#        print '--\n', report

    def render(self, suite):
#        w = self.width - 30
#        print self.width, int(w*0.3), int(w*0.7)
#        self.report.append('-'*30 + (' '*int(w*0.3))+('-'*int(w*0.7))+'\n')

        if the_config.report_detail == 0:
            return ''

        retval = ''
        for taskcase in sorted(suite, key=attrgetter('fname')):
            retval += self.build_taskcase(taskcase)

        retval += self.build_stats(suite)
        return retval


    def pretty(self, value):
        return "[%s]" % STR_STATUS[value]

    def join(self, lines):
        return str.join('\n', lines)

    def build_taskcase(self, case):
        retval = ''
        elapsed = "{0:6.2f}".format(case.elapsed)
        if case.elapsed > 1:
            elapsed = cout(RED, elapsed, NORM)

        TESTCASE_FORMAT = '{result} TaskCase: {path:<50} {time}\n'
        retval += TESTCASE_FORMAT.format(
            id =     id(case),
            result = pretty(case.result),
            path   = relpath(case.fname),
            time   = elapsed if the_config.case_time else '',
#            para   = 'P' if case.concurrent else '',
            )

        if case.result in [OK,NOEXEC] and the_config.report_detail < 2:
            return retval

        retval += self.tree.draw(case.tasks, callback=self.build_task)
        return retval


    def build_task(self, task, conn, tree, level=0):
        retval = ''
        #FIXME: retcode y cmd son específicos de Subprocess y deberían especializarse

        if not hasattr(task, 'retcode'):
            retcode = 'na'
        elif task.retcode is None:
            retcode = '-'
        else:
            retcode = int(task.retcode)

        if task.desc:
            try:
                desc = task.desc.decode('utf8', 'replace')
            except UnicodeDecodeError:
                desc = u"--"

            width = self.width - 28
            description = u"{0} | {1}".format(
                ellipsis(task.auto_desc.decode('utf8', 'replace')),
#                ellipsis(task.auto_desc, width*0.3-(3*level), True),
#                ellipsis(desc, width*0.7))
                ellipsis(desc))

        else:
#            description = ellipsis(task.auto_desc, self.width-28)
            description = ellipsis(task.auto_desc, width=self.width)

        TASK_SUMMARY = u"{result} {tree} {type}-{index:<3}{mode}({retcode:>2}:{expected:2})  {desc}\n"
        retval += TASK_SUMMARY.format(
            result   = self.pretty(task.result),
            tree     = tree,
            retcode  = retcode,
            expected = task.expected,
            mode     = task.get_mode(),
            type     = task.acro,
            index    = task.indx,
            desc     = description
            )

        if (the_config.report_detail > 2 or task.result != OK) and \
                isinstance(task, CompositeTask):
            retval += self.tree.draw(task.children, callback=self.build_task,
                                     connector=conn, args=[level+1])

        if task.conditions_ok and (the_config.report_detail < 4):
            return retval

        retval += self.tree.draw(task.pre, callback=self.build_condition,
                                 connector=conn, args=['pre', task])
        retval += self.tree.draw(task.post, callback=self.build_condition,
                                 connector=conn, args=['post', task])
        return retval


    def build_condition(self, condition, conn, tree, kind, task):

        CONDITION_SUMMARY = u"{value} {tree}{auto}{kind:<5} {name} {info}\n"
        return CONDITION_SUMMARY.format(
            value  = self.pretty(condition.value),
            tree   = tree,
            auto   = '+' if condition.auto else ' ',
            kind   = 'pre:' if condition in task.pre else 'post:',
            name   = condition.name,
#           info   = ellipsis(condition.basic_info(), self.width-33)
            info   = condition.basic_info()
            )


    def build_stats(self, suite):
        retval = ''

        delta = time.time() - the_config.init_time
        if delta > 60:
            t = time.strftime("%H:%M:%S", time.gmtime(delta))
        else:
            t = "%.2fs" % delta

        if suite.ok == suite.total:
            return "{0} - {1} - {2} - {3} \n".format(
                cout(BOLD, LIGHT_GREEN_BG, "[  ALL OK!  ]", NORM),
                t,
                count(suite.total, 'test'),
                count(suite.num_tasks, 'task'))

        return "{0} - {1} - {2}/{3}\n".format(
            cout(BOLD, LIGHT_RED_BG, "[    FAIL   ]", NORM),
            t, high(suite.ok),
            count(suite.total, 'test'))



# FIXME: la representación debería estar delegada a una clase que se pueda especializar
def task_view(task):
    return u"%s: %s(%s)" % \
        (high(task.name), task.__class__.__name__, ellipsis(task.str_param()))


class FailReporter(Reporter):
    '''A reporter to use only on fails'''

    @classmethod
    def get_options(cls, config, parser):

        if parser.has_option('--notify-ok'):
            return []

        return [optparse.Option('--notify-ok', action='store_true',
                                help='Send notifications even when all is right')]



    def do_render(self, suite):
        if not suite.all_ok() or the_config.notify_ok:
            Reporter.do_render(self, suite)

    def render(self, suite):
        try:
            user = os.getlogin()
        except OSError:
            user = 'atheist'

        host = socket.gethostname()

        retval = "Atheist: %s %s@%s:/%s\n" % (
            self.pretty(suite.all_ok()),
            user, host,
            the_config.basedir
            )

        if not suite.all_ok():
            retval += "SOME TASKS FAILED!\n"

        retval += '''
user: %s
host: %s
date: %s
argv: %s
base: %s
\n''' % (user, host,
         time.asctime(),
         str.join(' ', sys.argv),
         the_config.basedir)

        retval += Reporter.render(self, suite)
        return retval


class ConsoleReporter(Reporter):
    def __init__(self, dst='console'):
        Reporter.__init__(self, dst)
        self.width = the_config.screen_width

    def pretty(self, value):
        return pretty(value)


class TypedList(list):
    def __init__(self, cls, *values):
        self._cls = cls
        for i in values:
            self += i

    def append(self, val):
        Log.error("Use operator += instead")

    def __iadd__(self, val):
        if isinstance(val, list) or isinstance(val, tuple):
            for i in val: self += i
            return self

        check_type(val, self._cls)
        list.append(self, val)
        return self

    def copy(self):
        retval = TypedList(self._cls)
        retval.extend(self)
        return retval

    def prepend(self, val):
        self.insert(0, val)

    @classmethod
    def isA(cls, val, klass):
        "checks 'val' is a TypedList(cls)"
        assert isinstance(val, list), val
        for i in val:
            assert isinstance(i, klass)


class ConditionList(TypedList):
    def __init__(self, values=[]):
        TypedList.__init__(self, Condition)
        for i in values:
            self += i

    def __iadd__(self, val):
        if val in self:
            Log.debug("Trying to add a duplicate condition, ignored")
            return self
#        print "adding", id(val), val
        check_type(val, self._cls)
        return TypedList.__iadd__(self, val.clone())

    def __add__(self, other):
        retval = ConditionList()
        for i in self: retval += i
        for i in other: retval += i
        return retval

    def clone(self):
        return ConditionList(self)

    def remove_dups(self):
        return ConditionList(remove_dups(self))

    def replace(self, old, news):
        retval = ConditionList()
        for c in self:
            if c == old:
                retval.extend(news)
            else:
                retval += c

        return retval

    def __str__(self):
        retval = '<ConditionList \n['
        for x in self:
            retval += '%s:%s\n ' % (id(x), x)
        retval += ']>'
        return retval


class Condition(object):
    def __init__(self):
        self._value = NOEXEC
        self._task = None
        self.auto = False

    @property
    def value(self):
        return self._value

    def pre_task_run(self, task, condlist):
        """ Executed just BEFORE task execution """

        if self._task is not None and task is not self._task:
            Log.error("Conditions can not be shared: %s in %s, use clone()" % (self, self._task))
            sys.exit(1)

        self._value = NOEXEC
        self._task = task
        return []

    def run(self):
        raise NotImplemented

    def evaluate(self):
        self._value = self.run()
        assert self._value is not None
        return self._value

    def clone(self):
        retval = copy.copy(self)
        retval._value = NOEXEC
        retval._task = None
        return retval

    def __eq__(self, other):
        return isinstance(other, self.__class__)

    def __repr__(self):
        return str(self)

    def __str__(self):
        return unicode(self).encode('utf-8', 'replace')

    def __unicode__(self):
        return u"<%s %s %s>" % (pretty(self._value), self.name, self.basic_info())


    @property
    def name(self):
        return self.__class__.__name__

    def basic_info(self):
        return u''

    def more_info(self):
        return u''


class Callback(Condition, Public):
    @accept(func=types.FunctionType, args=tuple)
    @attributes('func', 'args')
    def __init__(self, func, args=()):
#        self.func = func
#        assert isinstance(args, tuple)
#        self.args = args
        Condition.__init__(self)

    def run(self):
        try:
            retval = self.func(*self.args)
            if retval is None:
                Log.warning("%s() returns None" % self.func.__name__)
            return bool(retval)

        except Exception, e:
            Log.error("%s: %s" % (self.__class__.__name__, e))
            print_exc(traceback.format_exc(limit=1), Log)

        return FAIL

    def __eq__(self, other):
        return Condition.__eq__(self, other) and \
            self.func == other.func and \
            self.args == other.args

    def basic_info(self):
        return u"%s: '%s'" % (self.func.__name__, self.args)



class EnvVarDefined(Condition, Public):
    @accept(varname=str)
    @attributes('varname', 'varvalue')
    def __init__(self, varname, varvalue=None):
        Condition.__init__(self)
#        self.varname = varname
#        self.varvalue = varvalue

    def run(self):
        retval = self.varname in os.environ.keys()
        if retval and self.varvalue:
            retval = (os.environ[self.varname] == self.varvalue)
        return retval

    def __eq__(self, other):
        return Condition.__eq__(self, other) and \
            self.varname == other.varname and \
            self.varvalue == other.varvalue

    def basic_info(self):
        return u"'%s'" % self.varname


class FileExists(Condition, Public):
    @accept(fname=str)
    def __init__(self, fname):
        self.fname = fname
        Condition.__init__(self)

    def __eq__(self, other):
        return Condition.__eq__(self, other) and self.fname == other.fname

    def run(self):
        return os.path.exists(self.fname)

    def basic_info(self):
        return u"'%s'" % relpath(self.fname)


class DirExists(FileExists, Public):
    def run(self):
        return FileExists.run(self) and os.path.isdir(self.fname)


class FileContains(Condition, Public):

    @accept(strip=str, whole=bool, times=int)
    @attributes('strip', 'whole', 'times')
    def __init__(self, expected, fname=None,  strip='', whole=False, times=1):
        assert fname != '', "'fname' must be None or non-empty string"

        self.fname = fname
#        self.strip = check_type(strip, str)
#        self.whole = check_type(whole, bool)
#        self.times = check_type(times, int)

        if whole:
            assert isinstance(expected, str), "In whole-mode, expected must be a string"

        self.expected = self.check_contains(expected)
        Condition.__init__(self)


    def pre_task_run(self, task, condlist):
        task.log.debug(u'pre-run %s' % self)
        retval = Condition.pre_task_run(self, task, condlist)

        if self.fname is None:
            task.enable_outs(enable_stdout=True)
            self.fname = task.stdout
            return retval

        exists = FileExists(self.fname)
        exists.auto = True

        if exists in condlist:
            return retval

        retval.extend(exists.pre_task_run(task, condlist))
        return retval + [exists]


    def check_contains(self, expected):
        if isinstance(expected, str):
            return [expected]
        elif isinstance(expected, list):
            assert all([isinstance(x, str) for x in expected]),\
                "FileContains args must be strings"
            return expected

        raise TypeError(expected)


    def run(self):
        if self.fname is None:
            Log.critical("Condition is not initialized!")
            return ERROR

        if not os.path.exists(self.fname):
            Log.error("'%s' does not exist" % relpath(self.fname))
            return ERROR

        try:
            fd = open(self.fname)
            fcontent = fd.read()
            fd.close()
        except IOError, e:
            Log.error(e)
            return ERROR

        if self.strip:
            fcontent = fcontent.strip(self.strip)

        if self.whole:
            return fcontent == self.expected[0]

        #FIXME: check expected order
        return all(self.check(x, fcontent) >= self.times for x in self.expected)


    @classmethod
    def check(cls, expected, content):
        return content.count(expected)


    def __eq__(self, other):
        return Condition.__eq__(self, other) and \
            self.fname == other.fname and \
            self.expected == other.expected and \
            self.times == other.times and \
            self.whole == other.whole and \
            self.strip == other.strip

    def basic_info(self):
        return u"'%s' (%s) content:'%s'" % (self.fname, self.times, self.expected)


class FileEquals(Condition, Public):
    def __init__(self, fname1, fname2=None):
        self.fname1 = fname1
        self.fname2 = fname2
        Condition.__init__(self)

    def pre_task_run(self, task, condlist):
        retval = Condition.pre_task_run(self, task, condlist)
        if self.fname2 == None:
            task.enable_outs(enable_stdout=True)
            self.fname2 = task.stdout

        return retval

        # auto-prepend a FileExists condition
# FIXME
#        for fname in [self.fname1, self.fname2]:
#            condlist.auto += FileExists(fname)

    def __eq__(self, other):
        return Condition.__eq__(self, other) and \
            self.fname1 == other.fname1 and \
            self.fname2 == other.fname2

    def run(self):
        return not os.system('diff %s %s > /dev/null' % (self.fname1, self.fname2))

    def basic_info(self):
        return u"'%s' == '%s'" % (self.fname1, self.fname2)


class FileIsNewerThan(Condition, Public):
    def __init__(self, fname1, fname2):
        self.fname1 = fname1
        self.fname2 = fname2
        Condition.__init__(self)

    def run(self):
        time1 = os.stat(self.fname1)[stat.ST_MTIME]
        time2 = os.stat(self.fname2)[stat.ST_MTIME]
        return int(time1 > time2);

    def __eq__(self, other):
        return Condition.__eq__(self, other) and \
            self.fname1 == other.fname1 and \
            self.fname2 == other.fname2

    def basic_info(self):
        return u"'%s' is newer than '%s'" % (self.fname1, self.fname2)


class ProcessRunning(Condition, Public):
    def __init__(self, ps):
        Condition.__init__(self)

        self.info = str(ps)
        if isinstance(ps, int):
            self.pid = ps
            return

        if isinstance(ps, str):
            self.pid = int(commands.getoutput('pidof %s' % ps).split()[0])
            return

        raise AssertionError, "ps must be int(PID) of str(program name)"


    def run(self):
        return process_exists(self.pid)
#        return os.system("ps ef %s > /dev/null" % self.pid) == 0

    def __eq__(self, other):
        return Condition.__eq__(self, other) and \
            self.pid == other.pid

    def basic_info(self):
        return u"'%s'" % self.info


class AtheistVersion(Condition, Public):
    def __init__(self, version):
        self.version = self.str_to_float(version)
        Condition.__init__(self)

    def run(self):
        Log.debug("%s: inst:%s req:%s" %
                   (self.__class__.__name__, VERSION, self.version))
        return self.str_to_float(VERSION) >= self.version

    @staticmethod
    def str_to_float(string):
        retval = str.join('.', string.split('.')[:2])
        try:
            retval = float(retval)
        except ValueError, e:
            Log.error(e)
            sys.exit(1)

        return retval


#FIXME: Clonación de Condition decoradas
class ConditionDecorator(Condition, Public):

    @accept(conds=Condition)
    def __init__(self, *conds):
        Condition.__init__(self)
        self.children = ConditionList(conds)
#        self.children = ConditionList()
#        for c in conds:
#            check_type(c, Condition)
#            self.children += c

    def clone(self):
        retval = Condition.clone(self)
        retval.children = self.children.clone()
        return retval

    def pre_task_run(self, task, condlist):
        retval = []
        for c in self.children:
            retval.extend(c.pre_task_run(task, condlist))
        retval.extend(Condition.pre_task_run(self, task, condlist))
        return retval

    def run(self):
        raise NotImplementedError

    def __eq__(self, other):
        return Condition.__eq__(self, other) and set(self.children) == set(other.children)

    @property
    def name(self):
        return "%s (%s" % (self.__class__.__name__,
                          str.join(' ', [x.name for x in self.children]))

    def basic_info(self):
        return unicode(str.join(' ', [x.basic_info() for x in self.children]))

    def more_info(self):
        return unicode(str.join(' ', [x.more_info() for x in self.children]))


class Not(ConditionDecorator, Public):
    def __init__(self, condition):
        ConditionDecorator.__init__(self, condition)

    def run(self):
        val = self.children[0].run()
        if isinstance(val, bool):
            return not(val)

        return ERROR


class Poll(ConditionDecorator, Public):
    def __init__(self, condition, interval=1, timeout=5):
        ConditionDecorator.__init__(self, condition)
        self.interval = interval
        self.timeout = timeout

    def run(self):
        enlapsed = 0
        while 1:
#            if self._task.mng.abort: break
            Log.debug("Polling condition: %s" % self.children[0])
            retval = self.children[0].run()
            if retval != FAIL:
                return retval

            if self.timeout is not None and enlapsed > self.timeout:
                break

            time.sleep(self.interval)
            enlapsed += self.interval

        return FAIL


class RemoteTestFactory(Public):
    def __init__(self, account):
        self.account = account
        try:
            self.user, self.host = self.account.split('@')
        except ValueError:
            self.host = self.account
            self.user = os.getlogin()

    def __call__(self, cmd, **values):
        values.update({'shell':True})
        return Test('ssh %s "%s"' % (self.account, cmd), **values)



# class BufFile:
#
#     def __init__(self, fd):
#         self.data = ''
#         self.fileno = fd.fileno()
#
#     def readline(self):
#         new = os.read(self.fileno, 2048)
#         if not new:
#             yield self.data
#             raise StopIteration
#
#         self.data += new
#         if not '\n' in self.data:
#             yield ''
#             raise StopIteration
#
#         for line in self.data.split()[:-1]:
#             yield line



class FileWrapper:

    def __init__(self, console_out, file_handler, tag, only_on_fail=False):
        self.prefix = '%s| ' % tag
        self.put_tag = True

        def file_write_tag(text, fd=sys.__stdout__):
            if len(text) == 0:
                return

            if self.put_tag:
                text = self.prefix + text
                self.put_tag = False

            out = text[:-1].replace('\n', '\n'+self.prefix) + text[-1]
            if text[-1] == '\n':
                self.put_tag = True

            fd.write(out.encode('utf-8'))
            fd.flush()

        def file_write(text, fd):
            fd.write(text)
            fd.flush()

        #--

        self.out_funcs = []
        self.last_task_out = None

        if console_out:
            self.out_funcs.append(file_write_tag)

        elif only_on_fail:
            self.last_task_out = StringIO.StringIO()
            self.out_funcs.append(partial(file_write_tag, fd=self.last_task_out))

        if file_handler:
            self.out_funcs.append(partial(file_write, fd=file_handler))

    def write(self, data):
        for func in self.out_funcs: func(data)

    def flush(self):
        pass

    def print_fail_out(self):
        if not self.last_task_out:
            return # it was not asked

        print self.last_task_out.getvalue(),
        sys.__stdout__.flush()


class Task(Record):

    acro = 'Task'
    allows = ['cwd', 'dirty', 'must_fail', 'save_stdout', 'save_stderr', 'stderr', 'stdout']
    forbid = []
    concurrent = True

    @classmethod
    def validate_kargs(cls, _dict):
        allways_allowed = ['delay', 'desc', 'loglevel', 'template', 'tid', 'todo']
        redundant = set(cls.allows).intersection(allways_allowed)
        assert not redundant, \
            "{0} can not be in '{1}.allows' specification".format(list(redundant), cls.__name__)

        allowed =  set(allways_allowed).union(cls.allows).difference(cls.forbid)

        for k,v in _dict.items():
            assert task_attrs.has_key(k), "'%s' is not a valid keyword" % k
            assert k in allowed, "'%s' is not an allowed keyword for '%s'" % (k, cls.__name__)

            check_type(v, task_attrs[k].type_)

#            if v == task_attrs[k].default:
#                Log.info("Default value for '%s' is already '%s'. Remove it." % (k, v))

        return _dict


    def __init__(self, mng, **kargs):
        self.mng = mng

        #current_ids = [x.id for x in TC] + [x.id for x in ts]  # All
        current_ids = [x.tid for x in self.mng.ts]    # Only this testcase

        assert 'tid' not in kargs.keys() or \
            kargs['tid'] not in current_ids, "Duplicate task id: '%s'" % kargs['tid']

        try:
            self.ts = kargs['parent'].children
        except KeyError:
            self.ts = self.mng.ts

        self.ts.append(self)
        self.kargs = kargs

        defaults = {}
        for k,v in task_attrs.items(): defaults[k] = v.default
        self.__dict__.update(defaults)

        if kargs.has_key('template'):
            check_type(kargs['template'], list)
            for i in kargs['template']:
                self.__dict__.update(self.validate_kargs(i))

        self.__dict__.update(self.validate_kargs(kargs))

        self.auto_desc = ''
        self._indx = self.mng.suite.next()
        self.fname = ''
        self.gen = TypedList(str)
        self.pre =  ConditionList()
        self.post = ConditionList()

        self._mode = None               # normal, setup, teardown
        self.result = NOEXEC
        self.conditions_ok = True

        self.thread = None
        self.time_start = None
        self.time_end = None

#        self.already_setup = False
        self.stderr_fd = None
        self.stdout_fd = None
        self.last_task_out = None


        self.save_stdout |= ((self.stdout != '') or the_config.save_stdout)
        self.save_stderr |= ((self.stderr != '') or the_config.save_stderr)

        self.enable_outs(self.save_stdout, self.save_stderr)

        self.wrap_outs = False

        #- logger
        self.log = logging.getLogger(self.name)
        if self.log.handlers:
            return # the logger is already setup

        term = logging.StreamHandler()
        term.setFormatter(atheist.log.CapitalLoggingFormatter(\
                the_config.timetag + '[%(levelcapital)s] %(name)s: %(message)s',
                log.DATEFORMAT))

        if self.loglevel is not None:
            loglevel = self.loglevel
        else:
            loglevel = the_config.loglevel

        term.setLevel(loglevel)
        self.log.setLevel(loglevel)
        self.log.addHandler(term)
        self.log.propagate = 0

        self.persistent_log = StringIO.StringIO()
        persistent_handler = logging.StreamHandler(self.persistent_log)
        persistent_handler.setLevel(logging.WARNING)
        self.log.addHandler(persistent_handler)


    def get_mode(self):
        return self._mode

    def set_mode(self, mode):
        self._mode = mode


    def enable_outs(self, enable_stdout=False, enable_stderr=False):

        def enable_out(task, name):
            assert name in ['stdout', 'stderr'], "Internal Error"

            out = getattr(task, name)
            if not out:
                out = os.path.join(ATHEIST_TMP, '%s.%s' % (task.name, name[-3:]))
                setattr(task, name, out)

            setattr(task, "save_%s" % name, True)
            task.gen += out

        if enable_stdout: enable_out(self, 'stdout')
        if enable_stderr: enable_out(self, 'stderr')


    # private
    def eval_conditions(self, condition_set, name):
        for c in condition_set:
            value = c.evaluate()
            self.log.info(u"%s: %s", name, c)
            if value == OK:
                continue

            self.conditions_ok = False
            self.result = FAIL
            if not the_config.keep_going:
                break


    def configure(self):
        '''Configure the task, reading config file or similar'''
        pass


    # final
    # FIXME: Debería dejar la Task en el estado inicial, necesario para --until-failure
    def do_pre_run(self):
        self.log.debug('pre-run')
        self.configure()
        self.pre_run()
        self.eval_conditions(self.pre, "Pre ")


    def pre_run(self):
        def extract_aditional_conditions(condlist):
            retval = ConditionList()
            for c in condlist:
                conditions = c.pre_task_run(self, condlist)
                assert conditions is not None, c
                retval.extend(conditions+[c])
            return retval

        self.pre = extract_aditional_conditions(self.pre)
        self.post = extract_aditional_conditions(self.post)

        self.pre = self.pre.remove_dups()
        self.post = self.post.remove_dups()
        self.gen = remove_dups(self.gen)

        self.gen = [os.path.abspath(x) for x in self.gen]

        for i in self.gen:
            exists = FileExists(i)
            exists.auto = True

            not_exists = Not(exists)
            not_exists.auto = True

            if not Not(exists) in self.pre:
                self.pre.prepend(not_exists)

            if not exists in self.post:
                self.post.prepend(exists)


    # final
    def do_post_run(self):
#        self.post_run()

        if self.result != OK:
            return

        self.eval_conditions(self.post, "Post")


#    YAGNI
#    def post_run(self):
#        pass

    @property
    def indx(self):
        return self._indx

    @property
    def name(self):
        return self.acro[0] + str(self.indx)


    def describe(self, pre_keys=[]):
        self.pre_run()

        dic = self.__dict__.copy()
        dic['fname'] = relpath(dic['fname'])

        keys = pre_keys[:] + ['fname']
        if self.result is not NOEXEC: keys.append('result')

        # add standard attrs with no-default value
        for k,v in task_attrs.items():
            if getattr(self,k) != v.default:
                keys.append(k)

        attrs = []
        for i in keys:
            value = dic[i]
            if isinstance(value, str):
                value = "'%s'" % value.replace('\n', '\n' + ' '*16)
                value = value.decode('utf8', 'replace')
            attrs.append(u"%s: %s" % (i.rjust(14), value))

        # multivalued attributes
        for a in ['pre','post','gen']:
            for i in getattr(self, a):
                attrs.append(u"{0:>14}: {1}".format(a, i))

        retval = u"%3s:%s\n%s\n" % (self.indx, self.__class__.__name__,
                                    str.join('\n', attrs))

#        return retval.encode('utf-8')
        return retval

    def __repr__(self):
        return str(self)

    def __str__(self):
        return unicode(self).encode('utf-8', 'replace')

    def __unicode__(self):
        return u"%s: <%s(%s)>" % \
            (self.name, self.__class__.__name__, self.str_param())

    def str_param(self):
        return u''

    def clone(self, *args):
        kargs = self.kargs
        kargs['desc'] = '(copy of %s) %s' % (self.name, kargs['desc'])

        retval = self.__class__(*args, **kargs)

        for c in self.pre:
            retval.pre += c
        for c in self.post:
            retval.post += c
        retval.gen = self.gen.copy()
        return retval


    def do_run(self):
        # key-val sanity
        if self.dirty and not self.gen:
            self.log.warning("'dirty' without 'gen' has no sense.")

        time.sleep(self.delay)

        if self.stdout:
            self.stdout_fd = codecs.getwriter('utf-8')(open(self.stdout, 'w', 0))

        if self.stderr:
            self.stderr_fd = codecs.getwriter('utf-8')(open(self.stderr, 'w', 0))


        self.outwrap = FileWrapper(the_config.stdout,
                                   self.stdout_fd,
                                   tag='%s:out' % self.name,
                                   only_on_fail=the_config.out_on_fail)

        self.errwrap = FileWrapper(the_config.stderr,
                                   self.stderr_fd,
                                   tag='%s:err' % self.name,
                                   only_on_fail=the_config.out_on_fail)

        try:
            if self.wrap_outs:
                sys.stdout = self.outwrap
                sys.stderr = self.errwrap

            self.tini = time.time()
            self.result = self.run()

            assert self.result is not None, "%s." % self.__class__.__name__

        except select.error:
            self.mng.abort()

        finally:
            sys.stdout = sys.__stdout__
            sys.stderr = sys.__stderr__

        if self.result == FAIL and not self.check:
            self.result = SOFT_FAIL

        if self.result not in [OK, SOFT_FAIL]:
            self.outwrap.print_fail_out()
            self.errwrap.print_fail_out()

        self.outwrap = None
        self.errwrap = None

        if self.stdout_fd:
            self.stdout_fd.close()

        if self.stderr_fd:
            self.stderr_fd.close()

        self.close()


    def is_running(self):
        raise NotImplementedError, self

    def run(self):
        raise NotImplementedError, self

    def terminate(self):
        raise NotImplementedError, self

    def close(self):
        '''Close file descriptors: stdin, stdout, etc.'''


class Subprocess(Task):

    allows = Task.allows + ['detach', 'env', 'expected', 'path', 'timeout', 'shell', 'signal']

    @accept(mng=Manager, cmd=(str, types.NoneType))
    def __init__(self, mng, cmd, **kargs):
#        print "%s '%s'" % (mng, cmd)
        self.cmd = '' if cmd is None else cmd

#        assert isinstance(self.cmd, (str, types.NoneType))

        Task.__init__(self, mng, **kargs)
        self.auto_desc = self.cmd

        # add the user variables to dft environ
        environ = os.environ.copy()
        for k,v in self.env.items():
            environ[k] = string.Template(v).safe_substitute(environ)

        self.env = environ
        self.ps = None
        self.retcode = None


    def run(self):
        if not self.cmd:
            return OK

        if self.path:
            os.environ['PATH'] += ':'+self.path

        if self.shell:
            cmd = ['/bin/bash', '-c', "%s" % self.cmd]
        else:
            # FIXME: test that
            if set('><|;&').intersection(set(self.cmd)):
                self.log.warning("The command '%s' has special characters." % self.cmd)
                self.log.warning("Perhaps it requires 'shell=True'")

            cmd = self.cmd.split()

        try:
            self.ps = subp.Popen(
                cmd,
                close_fds  = True,
                stdin      = subp.PIPE,
                stdout     = subp.PIPE,
                stderr     = subp.PIPE,
                shell      = False,
                bufsize    = 0,
                cwd        = self.cwd,
                env        = self.env,
                preexec_fn = os.setsid)

        except OSError, e:
            self.log.error("%s: '%s'" % (e.strerror, self.cmd))
            return ERROR

        self.log.debug("starts (pid: %s)" % self.ps.pid)

        read_fds = [self.ps.stdout, self.ps.stderr]
        read_ready = select.select(read_fds,[],[], 0.05)[0]

        while self.ps.poll() is None or read_fds:
            for fd in read_ready:
# http://mail.python.org/pipermail/python-bugs-list/2001-March/004491.html
                time.sleep(0.01)
                data = os.read(fd.fileno(), 2048).decode('utf-8', 'replace')

                if not data:
                    read_fds.remove(fd)
                elif fd is self.ps.stdout:
                    self.outwrap.write(data)
                elif fd is self.ps.stderr:
                    self.errwrap.write(data)

            try:
                read_ready = select.select(read_fds,[],[], 0.2)[0]
            except select.error:
                self.mng.abort()
                return ERROR

            if self.timeout == 0 or time.time()-self.tini < self.timeout:
                continue

            self.log.debug("timeout expired (%.1f>%s), sending signal %s to %s" % \
                               (time.time() - self.tini, self.timeout,
                                self.signal, self.ps.pid))
            self.kill()


        self.outwrap.write(self.ps.stdout.read())
        self.errwrap.write(self.ps.stderr.read())
        self.retcode = self.ps.returncode

        if self.shell and self.retcode == 127:
            self.log.error("No such file or directory: '%s'" % self.cmd)
            return ERROR

        retval = (self.retcode == self.expected)
        self.log.debug("%s finish with %s" % (pretty(retval), self.retcode))

        if self.must_fail:
            retval = (self.retcode != 0)

        return retval


    def is_running(self):
        return self.ps is not None and self.ps.poll() is None

    def str_param(self):
        return "'%s'" % self.cmd.decode('utf-8','replace')

    def describe(self):
        return Task.describe(self, ['cmd'])

    def clone(self):
        return Task.clone(self, self.cmd)

    def terminate(self):
        self.kill(self.signal)

    def kill(self, n=None, assure=True):
        if n is None:
            n = self.signal

        self.log.debug("sending signal %s" % n)

        try:
            os.killpg(self.ps.pid, n)
        except OSError, e:
            if e.errno == 3: return # OSError(3, 'No such process')
            Log.debug("%s (%s)" % (e, self.ps.pid))
            return
        except AttributeError:
            self.log.warning("did not even starts")
            return

        if not assure:
            return

        tini = time.time()
        while 1:
            if self.ps.poll() is not None: break
            time.sleep(0.5)

            if (time.time() - tini) > 5:
                self.log.warning('is still alive after 5 seconds!')
                self.kill(signal.SIGKILL, assure=False)

            if (time.time() - tini) > 3:
                self.kill(n, assure=False)



    def close(self):
        if not self.ps: return
        self.ps.stdin.close()
        self.ps.stdout.close()
        self.ps.stderr.close()



class Test(Subprocess, Public):
    acro = 'Test'


class TestFunc(Task, Public):
    acro = 'Func'
    allows = ['expected', 'must_fail']
    concurrent = False

    def __init__(self, mng, func, args=(), **kargs):
        assert callable(func)
        Task.__init__(self, mng, **kargs)

        self.func = func
        self.args = args

        self.auto_desc = u"{0}({1})".format(
            func.__name__,
            str.join('', [str(x) for x in self.args]))



    def run(self):
        sys.stdout = self.outwrap
        sys.stderr = self.errwrap

        try:
            self.retcode = self.func(*self.args)

            if not isinstance(self.retcode, int):
                Log.error("Function '%s' should return an integer" % self.func.__name__)

            if self.retcode is None:
                return UNKNOWN

            retval = self.retcode == self.expected

        except Exception, e:
            self.log.error(e)
            return ERROR

        finally:
            sys.stdout = sys.__stdout__
            sys.stderr = sys.__stderr__


        if self.must_fail and retval == FAIL:
            return OK

        return retval


class Command(Subprocess, Public):
    acro = 'Cmnd'

    def __init__(self, mng, cmd, **kargs):
        Subprocess.__init__(self, mng=mng, cmd=cmd, **kargs)
        self.check = False


class CompositeTask(Task, Public):
    acro = 'Comp'
    allows = ['detach']

    """ The CompositeTask let you specify a predicate to evalue a set of tasks:

    CompositeTask(all, t1, t2, t3) is OK if all the tasks are OK.

    #--

    def at_least_two(tasks):
        return [bool(x) for x in tasks].count(True) >= 2

    CompositeTask(at_least_two, t1, t2, t3) is OK when 2 or more tasks are OK.
    """

    def __init__(self, mng, oper, *tasks, **kargs):
        self.oper = oper
        self.children = TypedList(Task, tasks)
        for t in self.children:
            assert t.detach == False, 'CompositeTask children must not detachable'

        Task.__init__(self, mng, **kargs)

        for t in tasks:
            t.parent = self
            self.ts.remove(t)

        self.auto_desc = u"{0} {1}".format(oper.__name__,
                                          [x.name for x in tasks])

    def set_mode(self, mode):
        self._mode = mode
        for t in self.children:
            t.set_mode(mode)

    def run(self):
        self.gen = merge_uniq(*[t.gen for t in self.children])

        for t in self.children:
            Log.info(t)
            run_task(t)

        retval = self.oper(t.result == OK for t in self.children if t.check)
        self.log.debug('%s finish' % pretty(retval))
        return retval

    def terminate(self):
        self.log.debug('Terminating child tasks')
        for t in self.children:
            t.terminate()

    def is_running(self):
        return any(x.is_running() for x in self.children)

    def str_param(self):
        return str([x.name for x in self.children])


task_attrs = SortedDict({
    'tid':         Record(type_=str,  default=None),
    'check':       Record(type_=bool, default=True),
    'cwd':         Record(type_=str,  default=None),
    'delay':       Record(type_=(int,float), default=0),
    'desc':        Record(type_=str,  default=''),
    'detach':      Record(type_=bool, default=False),
    'dirty':       Record(type_=bool, default=False),
    'env':         Record(type_=dict, default=os.environ.copy()),
    'expected':    Record(type_=int,  default=0),
    'loglevel':    Record(type_=int,  default=None),
    'misconfig':   Record(type_=bool, default=False),
    'must_fail':   Record(type_=bool, default=False),
    'parent':      Record(type_=CompositeTask, default=None),
    'path':        Record(type_=str,  default=''),
    'template':    Record(type_=list, default=[]),
    'timeout':     Record(type_=int,  default=5),
    'save_stderr': Record(type_=bool, default=False),
    'save_stdout': Record(type_=bool, default=False),
    'shell':       Record(type_=bool, default=False),
    'signal':      Record(type_=int,  default=signal.SIGKILL),
    'stderr':      Record(type_=str,  default=''),
    'stdout':      Record(type_=str,  default=''),
    'todo':        Record(type_=bool, default=False),
    })



class Daemon(Command):
    acro = 'Daem'
    forbid = ['detach', 'timeout']

    def __init__(self, mng, cmd, **kargs):
        Command.__init__(self, mng, cmd, **kargs)
        self.timeout = 0
        self.detach = True
        self.check = False


class Template(dict, Public):
    def __init__(self, **kargs):
        self.update(kargs)



def run_task(task, recurse=False, end_callback=lambda:None):

    if task.detach and recurse==False:
        task.log.debug("detaching")
        task.thread = ThreadFunc(run_task, (task,),
                                 {'recurse':True, 'end_callback':end_callback})
        time.sleep(0.0)
        return None

    try:
        task.result = UNKNOWN

        if task.pre or task.post:
            task.result = OK

        task.do_pre_run()

        if task.conditions_ok:
            task.do_run()

        task.do_post_run()

    except ConfigureError,e:
        task.log.error(e)
        if task.misconfig:
            task.check = False
            task.result = MISCONF
        else:
            task.result = ERROR

    if task.result in [FAIL, ERROR] and task.todo:
        task.check = False
        task.result = TODO

    end_callback()
    return task.result


class TaskCase(object):
    def __init__(self, mng, fname, template={}, mode=task_mode.MAIN):
        self.mng = mng
        self.ts = mng.ts
        self.fname = fname
        self.template = template

        self.name = os.path.splitext(relpath(fname))[0]
        self.testdir =  os.path.dirname(fname)

        self.elapsed = 0

        # [vars] section in the config file
        if hasattr(the_config, 'vars'):
            self.template.update(dict(the_config.vars.items()))

        self.template.update({
                'basedir':     relpath(),
                'fullbasedir': relpath.root,
                'testdir':     relpath(self.testdir),
                'fulltestdir': self.testdir,
                'dirname':     " ERROR: ['$dirname' is deprecated: use '$testdir' instead] ",
                'fname':       self.name,
                'testname':    os.path.basename(self.name),
                'atheist':     './athcmd.py --plain',
                'pid':         the_config.pid,
                'tmp':         ATHEIST_TMP})

        del self.ts[:]

        if not the_config.skip_hooks and mode==task_mode.MAIN:
            setup = os.path.join(self.testdir, SETUP)
            if os.path.exists(setup):
                self.process_file(setup, task_mode.SETUP)

        self.process_file(fname, mode)

        if not the_config.skip_hooks  and mode==task_mode.MAIN:
            teardown = os.path.join(self.testdir, TEARDOWN)
            if os.path.exists(teardown):
                self.process_file(teardown, task_mode.TEARDOWN)

        self.tasks = self.ts[:]
        self.result = NOEXEC

        self.concurrent = all(x.concurrent for x in self.tasks)


    def __repr__(self):
        return "<TaskCase {fname}:{result}>".format(**self.__dict__)


    def process_file(self, fname, mode):
        Log.debug("loading %s" % relpath(fname))

        before = self.ts[:]
        env = self.mng.exec_env.copy()
        env['ts'] = self.ts
        env['__file__'] = os.path.abspath(fname)

        exec_file(fname, env, None, self.template)

        for t in set(self.ts) - set(before):
            t.fname = fname
            t.set_mode(mode)

    def calculate_result(self):
#            msg = '{0}:\n{1}'.format(
#                self.fname,
#                ['%s.%s.%s' % (x.name, x.result,x.check) for x in self.tasks]
#                )
#            Log.warning(msg)

        checked = [x.result==OK for x in self.tasks if x.check]
        if checked:
            self.result = all(checked)

        else:
            nochecked = [x.result==OK for x in self.tasks]
            if all(nochecked):
                self.result = OK
            else:
                self.result = SOFT_FAIL

        if any(x.result == MISCONF for x in self.tasks):
            self.result = MISCONF

        if any(x.result == TODO for x in self.tasks):
            self.result = TODO


    def get_generated(self):
        retval = []

        for t in self.tasks:
            if t.result == NOEXEC: continue
            if the_config.clean and t.dirty:
                t.log.warning("dirty-task, not removing gen: %s" % t.gen)
            else:
                retval.extend(t.gen)

        return retval



class CaseResult:
    def __init__(self, identifier):
        self.fname = identifier
        self.result = NOEXEC
        self.elapsed = 0
        self.tasks = {}

    def __repr__(self):
        return '<CaseResult {0}: {1}>'.format(
            self.fname,
            str.join(',', [x.name for x in self.tasks])
            )

#class TaskResult(Record): pass

class CaseExecutor:
    def __init__(self, case, observer=None):
        self.case = case
        self.observer = observer
        self.mng = case.mng

    # template-method
    def run(self):
        tini = time.time()
        Log.info("Test case {0:-<{1}}".format(
                relpath(self.case.fname) + ' ',
                the_config.screen_width-15))

        if not self.case.tasks:
            return

        self.run_tasks()
        self.wait_detached()
        self.kill_daemons()
        self.case.calculate_result()
        the_config.reporters.report_case(self.case)
        self.mng.clean_async(
            os.path.join(ATHEIST_TMP, os.path.basename(self.case.name)),
            self.case.get_generated())

        self.case.elapsed = time.time() - tini


#    def async_run(self):
#        retval = CaseResult(
#            identifier = self.case.fname)
#
#        self.run()
#
#        for task in self.case.tasks:
#            retval.tasks[task.name] = TaskResult(
#                result = task.result
#                )
#
#        return retval


    def run_tasks(self):
        for t in self.case.tasks:
            if self.mng.aborted:
                break

            # FIXME: use per-case or specific logger?
            Log.info(task_view(t))
            result = run_task(t, end_callback=self.observer)

            if result in [FAIL,ERROR] and t.check \
                    and not the_config.keep_going:
                t.log.info("FAIL, skipping remaining tasks ('keep-going mode' disabled)")
                break


    def wait_detached(self):
        while 1:
            unfinished = [x for x in self.case.tasks if x.thread \
                              and x.thread.isAlive() \
                              and x.timeout != 0]

            if not unfinished:
                break

            for t in unfinished:
                t.log.debug("-- waiting to finish detached task")

                if self.mng.aborted:
                    t.terminate()

                t.thread.join(0.5)


    def kill_daemons(self):
        while 1:
            daemons = [x for x in self.case.tasks if x.thread \
                           and x.thread.isAlive() \
                           and x.timeout == 0]

            if not daemons or self.mng.aborted:
                break

            for t in daemons:
                t.log.debug("-- killing daemon")

                t.terminate()
                t.thread.join(0.5)



def load_dirty_filelist(dirname):
    retval = []

    for fname in glob.glob(dirname + '/*.gen'):
        Log.debug("reading generated files from '%s'" % fname)

        try:
            with file(fname) as fd:
                retval.extend([x.strip('\n') for x in fd.readlines()])
        except IOError, e:
            Log.error(e)
            continue

#    print "generated-logs", dirname, retval
    return retval


def get_generated_from_finished_atheists():
    retval = []
    for fname in os.listdir(ATHEIST_TMP_BASE):
        fpath = os.path.join(ATHEIST_TMP_BASE, fname)
        if not os.path.isdir(fpath):
            continue

        # don't remove running atheist tmp dirs
        if fname.isdigit() and process_exists(int(fname)):
            continue

        retval.extend(load_dirty_filelist(fpath))

        for f in [fpath] + [os.path.join(fpath, x) for x in os.listdir(fpath)]:
            if f not in retval:
                retval.append(f)

#    print '@', retval
#    base = load_dirty_filelist(ATHEIST_TMP_BASE)
#    print '@', base
#    retval.extend(base)

    return retval



def save_dirty_filelist(fname, filelist):
    if not filelist:
        return

    filelist = remove_dups([fname] + filelist)

    gen_fd = open(fname, 'w')
    Log.debug("Storing generated files in %s:\n%s" % (fname, filelist))

    for fname in filelist:
        Log.warning("dirty-mode: not removing '%s'" % fname)
        gen_fd.write(fname + '\n')

    gen_fd.flush()
    gen_fd.close()



def remove_files_and_dirs(paths):

    def cmp(f1, f2):
        f1dir = os.path.isdir(f1)
        f2dir = os.path.isdir(f2)
        if f1dir == f2dir:
            return 0
        if f1dir:
            return 1

        return -1

    paths.sort(cmp=cmp)


    for f in paths:
        f = os.path.abspath(f)
        if not (f.startswith('/tmp') or f.startswith(relpath.root)):
            Log.warning('Removing files out of /tmp or %s is forbbiden: %s',
                        relpath.root, f)
            continue

        if os.path.isdir(f):
            Log.debug("- removing directory '%s'" % relpath(f))
            try:
                os.rmdir(f)
            except OSError, e:
                Log.warning(e)

        elif os.path.isfile(f):
            Log.debug("- removing file '%s'" % relpath(f))
            try:
                os.remove(f)
            except OSError, e:
                Log.warning(e)
        else:
            Log.info("- removing file '%s': does not exist!" % relpath(f))




class Suite(list):
    def __init__(self):
        self._task_index = 0

        self.total = 0
        self.ok = 0
        self.failures = 0
        self.errors = 0
        self.skipped = 0

#    def append(self, taskcase):
#        tests = [x for x in taskcase.tasks if x.check]
#        self.total += len(tests)
#        self.ok     += len([x for x in tests if x.result == OK])
#        self.failures   += self.total - self.ok  # puede que no hayan sido ejecutados
##        Log.info("*** %s fail: %s", taskcase, self.failures)
#        list.append(self, taskcase)

    def calculate(self):
        tests = [x for x in self.itertasks() if x.check]
#        print tests
        self.total = len(tests)
        self.ok     = len([x for x in tests if x.result == OK])
        self.errors = len([x for x in tests if x.result == ERROR])
        self.failures = self.total - self.ok  # puede que no hayan sido ejecutados
        self.skipped  = len([x for x in tests if x.result is [NOEXEC]])

    def next(self):
        self._task_index += 1
        return self._task_index

    def itertasks(self):
        return chain(*[x.tasks for x in self])

    @property
    def num_tasks(self):
        return sum([len(s.tasks) for s in self])

    def all_ok(self):
        return self.ok == self.total

    def copy(self):
        retval = Suite()
        for case in self:
            retval.append(case)

        return retval


class Runner(object):
    def __init__(self, mng):
        Log.info("Using runner: %s", self)
        self.mng = mng
        self.setup_done = False
        self.suite = []

        if the_config.random is not None:
            if self.cfg.random == 0:
                random.seed()  # random seed
            else:
                random.seed(the_config.random)


    def setup(self):
        if self.setup_done:
            return

        self.setup_done = True

        self.mng.reload()
        self.suite = self.mng.suite

        if the_config.random is not None:
            random.shuffle(self.suite)

        self.setup_progress_bar()


    def setup_progress_bar(self):
        self.pb = ProgressBar(
            self.mng.suite.num_tasks,
            width=int(the_config.screen_width),
            label='Run ',
            disable= the_config.disable_bar or \
                the_config.verbosity or \
                the_config.quiet or \
                the_config.stdout or \
                the_config.stderr)

        self.pb.listen_logger(Log, the_config.loglevel)


    def run(self):
        self.run_suite()
        self.report()
        return self.suite.failures


    def run_suite(self):
        self.setup()
        self.pb.reset()
        for case in self.suite:
            executor = CaseExecutor(case, observer=self.pb.inc)
            executor.run()
        self.pb.clean()
        self.suite.calculate()
        return self.suite


    def report(self, suite=None):
        if suite is None:
            suite = self.suite

        for r in the_config.reporters:
            r.do_render(suite)

#    def clean(self):
#        generated = []
#        for case in self.suite:
#            generated.extend(case.get_generated())
#
#        generated.append(ATHEIST_TMP)
#
#        if the_config.clean:
#            remove_files_and_dirs(generated)
#        else:
#            save_dirty_filelist(
#                os.path.join(ATHEIST_TMP, 'dirty.list'),
#                generated)
#


#    def async_run_suite(self):
#        self.setup()
#        results = {}
#        for case in self.suite:
#            executor = CaseExecutor(case, observer=self.pb.inc)
#            results[case.fname] = executor.run()
#        self.aync_apply_results(results)
#        self.suite.calculate()
#        return self.suite
#
#    def async_apply_results(self, results):
#        for case in self.suite:
#            result_case = results[case.fname]
#            case.result = result_case.result
#            for task in case:
#                result_task = resul_case[case.name]
#                task.result = result_task.result

    def __repr__(self):
        return "<{0}>".format(self.__class__.__name__)


class Counter:
    def __init__(self):
        self.n = 0
        self.lock = threading.Lock()

    def inc(self, *args):
        with self.lock:
            self.n += 1

    @property
    def value(self):
        return self.n


class ThreadPoolRunner(Runner):
    def run(self):

        def run_concurrent_suite(suite):
            done = Counter()

            pool = SimpleThreadPool(nworkers)

            for case in suite:
                executor = CaseExecutor(case, self.pb.inc)
                pool.add(executor.run, [], callback=done.inc)

            while not self.mng.aborted:
                time.sleep(0.5)
                if done.value == len(suite):
                    break

            pool.join()
#            pool.join(True, True)


        self.setup()
        nworkers = the_config.workers

        if len(self.suite) == 1 or nworkers == 1:
            return Runner.run(self)

        if nworkers == 0:
            nworkers = min(100, 1+len(self.suite)/2)
            Log.info("Creating %s workers" % nworkers)

        concurrent_suite = [x for x in self.suite if x.concurrent]

        self.pb.reset()
        run_concurrent_suite(concurrent_suite)

        not_concurrent_suite = [x for x in self.suite if not x.concurrent]
        for case in not_concurrent_suite:
            executor = CaseExecutor(case, self.pb.inc)
            executor.run()
        self.pb.clean()

        self.suite.calculate()

        self.report()
        return self.suite.failures





class DescribeRunner(Runner):
    def run(self):
        self.setup()
        for task in self.suite.itertasks():
            print task.describe().encode('utf8')

        return 0


class ListOnlyRunner(Runner):
    def run(self):
        self.setup()
        for case in self.suite:
            print(case.fname)
            for task in case.tasks:
                print (u'   %s' % task).encode('utf8')

        return 0
