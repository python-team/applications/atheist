# -*- mode: python; coding: utf-8 -*-

import pwd, os, re

VERSION = '0.20110402'

SETUP    = '_setup.test'
TEARDOWN = '_teardown.test'
DIR_SETUP = '_SETUP.test'
DIR_TEARDOWN = '_TEARDOWN.test'

SPECIAL = [SETUP, TEARDOWN, DIR_SETUP, DIR_TEARDOWN]

# FIXME: fichero de configuración .atheist
EXCLUDE = ['.svn', '.hg'] + SPECIAL

INCLUDE_RE = re.compile(r"""include\("([\./\w\d]+)"\)\s*$""")


#task status
FAIL =    False   # Failed
OK =      True    # Success
NOEXEC =  2       # Never executed
ERROR =   3       # The task itself produced an unexpected error
UNKNOWN = 4
TODO    = 5
SOFT_FAIL = 6
MISCONF = 7

STR_STATUS = {\
    FAIL:      'FAIL',
    OK:        ' OK ',
    NOEXEC:    ' -- ',
    ERROR:     ' !! ',
    UNKNOWN:   ' ?? ',
    TODO:      'ToDo',
    SOFT_FAIL: 'fail',
    MISCONF:   'conf',
    }

ATHEIST_BASE = os.path.join(os.environ['HOME'], '.atheist')
ATHEIST_TMP_BASE = os.path.join('/tmp', 'atheist-{0}'.format(pwd.getpwuid(os.getuid())[0]))
ATHEIST_TMP =  os.path.join(ATHEIST_TMP_BASE, str(os.getpid()))
ATHEIST_CFG =  os.path.join(ATHEIST_BASE, 'config')
ATHEIST_LOG =  os.path.join(ATHEIST_BASE, 'log')
ATHEIST_EXT = '.test'

USAGE = '''atheist [options] [file|directory]

atheist is a testing tool.
This is version %s, Copyright (C) 2009,2010,2011 David Villa Alises
atheist comes with ABSOLUTELY NO WARRANTY; This is free software, and you are
welcome to redistribute it under certain conditions; See COPYING for details.''' % VERSION
