import os
import socket

import atheist
from atheist.gvar import Log

class OpenPort(atheist.Condition, atheist.Plugin):
    def __init__(self, port, host='localhost', proto='tcp'):
        assert proto in ['tcp','udp']
        assert isinstance(port , int) and 0 < port < 65535
        atheist.Condition.__init__(self)

        self.proto = proto
        self.port = port
        self.host = host

        if self.proto == 'udp' and host != 'localhost':
            raise Exception('OpenPort does not support remote UDP ports')

    def run(self):
        if self.host == 'localhost':
            return self.localport()

        return self.remoteport()

    def __eq__(self, other):
        return atheist.Condition.__eq__(self, other) and \
            self.port == other.port and \
            self.host == other.host and \
            self.proto == other.proto

    def localport(self):
        return os.system('fuser -n %s %s > /dev/null 2> /dev/null' % \
                             (self.proto, self.port)) == 0

    def remoteport(self):
        #return os.system(self.cmd = 'nmap -p%s %s | grep "%s/%s open" > /dev/null' % \
        #                     (self.port, self.host, self.port, self.proto)) == 0

        s = socket.socket()
        s.settimeout(1)
        try:
            s.connect((self.host, self.port))
            s.shutdown(socket.SHUT_RDWR)
            s.close()
            return True
        except socket.error, e:
            Log.warning("OpenPort: %s" % e)
            return False

    def basic_info(self):
        return "'%s %s/%s'" % (self.host, self.proto, self.port)
