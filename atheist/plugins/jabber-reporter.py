# -*- mode:python; coding:utf-8; fill-column:110 -*-
import sys
import optparse

from loggingx import JabberHandler

import atheist
from atheist.gvar import Log

class JabberReporter(atheist.FailReporter, atheist.Plugin):
    def __init__(self, config, account):
        try:
            handler = JabberHandler((config.jabber.user,
                                     config.jabber.pasw),
                                    account, Log)
        except AttributeError, e:
            Log.error("You must give a jabber account in your config file. Key: %s" % e.args)
            sys.exit(1)

        atheist.FailReporter.__init__(self, config, 'jabber://'+account, handler)


    @classmethod
    def get_options(cls, config, parser):
        retval = atheist.FailReporter.get_options(config, parser)
        retval.append(optparse.Option(
                '--notify-jabber', dest='jabber_accounts', action='append', default=[],
                help='notify failed tasks to the given jabber account'))
        return retval


    @classmethod
    def config(cls, config, parser):
        for account in config.jabber_accounts:
            config.reporters.append(JabberReporter(mng, account))
