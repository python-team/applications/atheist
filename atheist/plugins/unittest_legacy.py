# -*- mode: python; coding: utf-8 -*-

# unittest python plugin for atheist
#
# Copyright (C) 2010 Oscar Aceña
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import sys, unittest, inspect, os

import atheist


class UnitTask(atheist.Task):
    acro = 'Unit'
    allows = ['check', 'expected', 'must_fail']

    def __init__(self, mng, utest, method, **kargs):
        assert callable(utest)
        self.utest = utest
        self.method = method

        atheist.Task.__init__(self, mng, **kargs)
        self.wrap_outs = True

        self.auto_desc = method.__name__.lstrip('test')
        if method.__doc__:
            self.auto_desc += ": " + method.__doc__.split("\n")[0].strip()


    def run(self):
        try:
            tr = unittest.TestResult()
            self.utest(tr)

            if tr.wasSuccessful():
                return atheist.OK

            # to Unittest failures are caused by incorrect results
            # and errors are caused by incorrect code
            if tr.errors:
                sys.stderr.write(tr.errors[0][1])
                return atheist.ERROR

            if tr.failures:
                sys.stderr.write(tr.failures[0][1])
                return atheist.FAIL

            return atheist.ERROR

        except Exception, e:
            self.log.error(e)
            return atheist.FAIL

    def str_param(self):
        return self.method.__name__


#FIXME: generar automáticamente a partir de ficheros que contienen unittest.TestCase
# como hace trial, py.test o python2.7. Quizá se podría usar trial.
class UnitTestCase(atheist.CompositeTask, atheist.Plugin):
    acro = 'UCas'
    concurrent = False

    def __init__(self, mng, klass):
        if not issubclass(klass, unittest.TestCase):
            raise TypeError, "Test cases should be derived from unitest.TestCase"

        methods = [x for x in inspect.getmembers(klass, inspect.ismethod) if
                   x[0].startswith("test")]

        children = [UnitTask(mng, klass(name), method) for name,method in methods]

        atheist.CompositeTask.__init__(self, mng, all, *children)
        self.auto_desc = unicode(klass.__name__)


#    @classmethod
#    def load_all(cls):
#
#        modulename = inspect.stack()[1][1]
#        basename = os.path.splitext(os.path.basename(modulename))[0]
#        module = sys.modules[basename]
#
#        print dir(module)
