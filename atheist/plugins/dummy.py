import atheist

class SampleTask(atheist.Task, atheist.Plugin):

    acro = 'Samp'

    def __init__(self, mng, **kargs):
        # here you can add/modify/modify kargs
        atheist.Task.__init__(self, mng, **kargs)
        # your code
        # Do not check out staff or execute nothing here. only attribute assignments


    def configure(self):
        ''' [optional]
        - Read/process specific config info (usually on files)
        - It may raise ConfigureError() when essential data is missing or wrong
        '''


    def pre_run(self):
        ''' [optional]
        - You can add automatic pre and post conditions here
        '''


    def is_running(self):
        ''' [mandatory] but possibly inherited
        - Only for non-blocking tasks
        '''
        return False


    def run(self):
        ''' [mandatory]
        - The operations that run the actual task
        - It must assign a valid value to self.result
        '''
        return atheist.OK


    def str_param(self):
        ''' [optional]
        Some brief info for the textual representation of the *instance*.
        Only a few words.
        '''
        return 'sample'


    def terminate(self):
        ''' [mandatory] but possibly inherited
        Only for non-blocking tasks
        '''



class SampleCondition(atheist.Condition, atheist.Plugin):

    def __init__(self, values):
        atheist.Condition.__init__(self)
        self.values = values
        # Store values only. Don't process nothing here!


    def pre_task_run(self, task, condlist):
        ''' [optional]
        This is invoked just BEFORE condition execution
        - task: where the condition is added.
        - condlist: the list of conditions where the condition is (pre/post)
        - pos: position in that list.
        '''
        return []

    def run(self):
        ''' [mandatory]
        It actually checks the condition.
        It must return a boolean con the condition value.
        '''
        return True


    def __eq__(self, other):
        ''' [mandatory]
        Equality with other condition instances. Usually is based on attributes.
        '''


    def basic_info(self):
        ''' [optional]
        Some brief info for the textual representation of the *instance*.
        Only a few words.
        '''
        return str(self.values)


    def more_info(self):
        ''' [optional]
        Some aditional details of the condition instance.
        '''
        return 'some details'
