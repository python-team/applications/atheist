# -*- mode: python; coding: utf-8 -*-

import atheist
from pyarco.Type import accept

class TaskFinished(atheist.Condition, atheist.Plugin):
    """Check if some tasks has been executed or not (pending of execution)"""
    #FIXME: TaskFinished is the same of Not(TaskRunning)???


    def __init__(self, *tasks):
        for t in tasks:
            assert isinstance(t, atheist.Task)
        self.the_tasks = list(tasks)[:]
        atheist.Condition.__init__(self)

    def run(self):
        return all([t.result in (atheist.FAIL, atheist.OK)
                    for t in self.the_tasks])

    def __eq__(self, other):
        return atheist.Condition.__eq__(self, other) and \
            set(self.the_tasks) == set(other.the_tasks)

    def basic_info(self):
        return "%s" %  [t.name for t in self.the_tasks]


class TaskRunning(atheist.Condition, atheist.Plugin):
    """Check whether some tasks are running"""
    # FIXME: solo sirve para Subprocess

    def __init__(self, *tasks):
        self.the_tasks = list(tasks)[:]
        atheist.Condition.__init__(self)

    def run(self):
#        return all([(t.ps is not None) and (t.ps.poll() is None)
#                    for t in self.the_tasks])
        return all(t.is_running() for t in self.the_tasks)

    def __eq__(self, other):
        return atheist.Condition.__eq__(self, other) and \
            set(self.the_tasks) == self.other(the_tasks)

    def basic_info(self):
        return "%s" % [t.name for t in self.the_tasks]


class TaskTerminator(atheist.Task, atheist.Plugin):
    """A special task to kill and ensure the termination of other tasks."""

    def __init__(self, mng, *tasks, **kargs):
        if isinstance(tasks, atheist.Task):
            tasks = [tasks]

        self.tasks = atheist.TypedList(atheist.Task)
        for t in tasks:
            self.tasks += t

        atheist.Task.__init__(self, mng, **kargs)

        # FIXME: aún no está fijado el indx
        self.auto_desc = "Terminates %s" % \
            ', '.join([x.name for x in self.tasks])

        for t in self.tasks:
            self.post += atheist.Poll(atheist.Not(TaskRunning(t)))

    def run(self):
        for t in self.tasks:
            self.log.debug("terminates %s sending signal %s" % \
                               (t.name, t.signal))
            t.terminate()

        return atheist.OK

    def str_param(self):
        return str([x.name for x in self.tasks])
