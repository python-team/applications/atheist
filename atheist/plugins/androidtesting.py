# -*- mode: python; coding: utf-8 -*-

import atheist

class DroidDeviceReady(atheist.Condition, atheist.Plugin):
    '''This condition evaluates the output of the command "adb
    devices". The command output is like the following example:
    \t$ adb devices
    \tList of devices attached
    \tdevice-name\tstatus
    \tdevice2-name\tstatus
    '''

    def __init__(self, devname='',  allowMultiple=False):
        '''If devname specified, ignore the allowMultiple arg.  Else,
        if allowMultiple is False, look for a single device
        connected. If allowMultiple es True, only test if there is
        some device connected, ignoring their device names
        '''
        self.__devname = atheist.check_type(devname, str)
        self.__allowMultiple = atheist.check_type(allowMultiple, bool)

        atheist.Condition.__init__(self)


    def run(self):
        import commands

        # skip the first and the two last elements of the list.
        # the first line is a description message of the command
        raw_devices = commands.getoutput('adb devices').strip().split('\n')[1:]
        devices = [ x.split('\t')[0] for x in raw_devices ]

        if self.__devname != '':
            return self.__devname in devices

        if self.__allowMultiple:
            return len(devices) > 0

        else:
            return len(devices) == 1

    def basic_info(self):
        if self.__devname != '':
            return "'%s' is connected" % self.__devname

        else:
            return "Some device is connected"


class DroidTest(atheist.Subprocess, atheist.Plugin):
    'Class to do tests to Android developments'

    acro = 'Andr'
    allows = ['check', 'save_stdout', 'stdout', 'device']

    def __init__(self, mng, *filters, **kargs):
        '''The DroidTest constructor get the following arguments:

filters: a dictionary or a list with tuples on the form (Tag, verbosity level).
device: a string with the device serial number to use. If not
        specified, try to use a single connected devices (it will fail
        if it's more than one device connected
\tThe verbosity level must be one of the Android defined: see adb
\tlogcat --help for info
'''
        filter_spec = ''

        for f in filters:
            filter_spec += str(f) + ' '

        cmd = 'adb '

        if kargs.has_key('device'):
            cmd += '-s %s ' % kargs.pop('device')

        cmd += 'logcat -d %s *:S' % filter_spec

        atheist.Subprocess.__init__(self, mng=mng, cmd=cmd, **kargs)
        self.shell = True

        self.auto_desc = 'DroidTest'
