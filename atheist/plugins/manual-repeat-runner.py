# -*- mode:python; coding:utf-8 -*-

import sys
import time
import optparse
from multiprocessing import Process

import atheist
from atheist.gvar import Log

from pyarco.UI import cls


class ManualRepeatRunner(atheist.Runner, atheist.Plugin):

    @classmethod
    def get_options(cls, config, parser):
        return [optparse.Option('--again', action='store_true',
                                help='Repeat tests when user press ENTER')]

    @classmethod
    def config(cls, config, parser):
        if not config.again:
            return

        config.RunnerClass = cls


    def subprocess(self):
        def simple_runner():
            try:
                atheist.Runner(self.mng).run()
            except atheist.ExecutionError, e:
                Log.error(e)

        p = Process(target=simple_runner)
        p.start()
        p.join()


    def run(self):
        while 1:
            cls()
            print time.asctime()

            self.subprocess()

            if self.mng.aborted:
                break

            try:
                raw_input("\n-- Press ENTER to run tests again... ")
            except EOFError:
                self.mng.abort()
                break

        return 0
