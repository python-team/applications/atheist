import os
import imp, inspect

import atheist
from atheist.utils import *

#from atheist.gvar import Log

log = logging.getLogger('atheist.plugin')
log.propagate = True
log.addHandler(atheist.log.NullHandler())

class Loader(object):
    def __init__(self, dname):
        self.plugins = []
        for m in self.load_modules(dname):

            log.debug("PluginManager: Loading module '%s'",  m.__file__)

            klases = [x[1] for x in inspect.getmembers(m, inspect.isclass)]
            if not klases:
                continue

            plugins = [x for x in klases if \
                           issubclass(x, atheist.Plugin) \
                           and x.is_enabled()]

            log.debug("PluginManager: %s" % [x.__name__ for x in plugins])
            self.plugins.extend(plugins)

        log.debug("Registered plugins: %s" % sorted([x.__name__ for x in self.plugins]))


    @staticmethod
    def load_modules(dirname):
        banned = ['notify_runner.py']
        files = [os.path.splitext(x)[0]  for x in os.listdir(dirname) \
                     if not x.startswith('.') and \
                     not x.startswith('#') and \
                     x.endswith('.py') and \
                     not x in banned]

        files.sort()
        modules = []

        for f in files:
            try:
                modules.append(Loader.load_module(f, dirname))
            except ImportError, e:
                log.error("Loading plugin '%s': %s", f, e)

        return modules


    @staticmethod
    def load_module(filename, dirname):
        fp, fpath, desc =  imp.find_module(filename, [dirname])
#        log.debug("Module: %s %s" % (fpath, desc))
        try:
            return imp.load_module(filename, fp, fpath, desc)
        finally:
            if fp: fp.close()


    def __iter__(self):
        return iter(self.plugins)
