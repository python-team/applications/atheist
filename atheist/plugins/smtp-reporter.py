# -*- mode:python; coding:utf-8 -*-
import sys
import socket
import optparse

from loggingx import SMTP_SSLHandler

import atheist
from atheist.gvar import Log


class MySMTPHandler(SMTP_SSLHandler):
    def getSubject(self, record):
        return record.msg.split('\n')[0]


class SMTP_Reporter(atheist.FailReporter, atheist.Plugin):
    def __init__(self, config, toaddrs):

        if not hasattr(config, 'smtp'):
            raise atheist.ConfigureError("Your config requires a section 'smtp'.")

        host = config.smtp.host
        port = int(config.safe_get('smtp.port', 25))

        try:
            fromaddr = config.smtp.user
            credentials = (fromaddr, config.smtp.pasw)
        except AttributeError:
            fromaddr = 'atheist@' + socket.gethostname()
            credentials = None

        ssl = config.safe_get('smtp.ssl', False)

        Log.debug('SMTP_Reporter.mailhost: {0}:{1} ssl:{2}'.format(host, port, ssl))
        Log.debug('SMTP_Reporter.user: {0}'.format(credentials))

        try:
            handler = MySMTPHandler((host, port), fromaddr, toaddrs,
                                    subject='Atheist report',
                                    credentials=credentials,
                                    ssl=ssl)
        except AttributeError, e:
            Log.error("You must give a smtp account in your config file. Key: %s" % e.args)
            sys.exit(1)
        except socket.error, e:
            Log.error("{0}: {1}", host, e)
            return

        atheist.FailReporter.__init__(self, config, 'smtp://'+toaddrs, handler)


    @classmethod
    def get_options(cls, config, parser):
#        retval = atheist.FailReporter.get_options(config, parser)
        retval = []
        retval.append(optparse.Option(
                '--notify-smtp', dest='mail_accounts', action='append', default=[],
                help='notify failed tasks to the given email address'))
        return retval


    @classmethod
    def config(cls, config, parser):
        for account in config.mail_accounts:
            config.reporters.append(SMTP_Reporter(config, account))
