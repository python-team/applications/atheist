# -*- mode:python; coding:utf-8; tab-width:4 -*-

import socket
import smtplib

import atheist
the_config = atheist.gvar.the_config

class SMTP(atheist.Task, atheist.Plugin):
    acro = 'SMTP'
    allows = ['misconfig']

    def __init__(self, mng, key='smtp', **kargs):
        self.key = key
        atheist.Task.__init__(self, mng, **kargs)
        self.conn = None

    def configure(self):
        key = self.key

        if not hasattr(the_config, key):
            raise atheist.ConfigureError("Your config requires a section '%s'." % key)

        try:
            self.host = the_config['{0}.host'.format(key)]
        except AttributeError, e:
            raise atheist.ConfigureError(
                "Your config requires a key '%s.%s'" % (key, e.args[0]))

        try:
            self.port = int(the_config.safe_get('{0}.port'.format(key),  smtplib.SMTP_PORT))
        except ValueError, e:
            raise atheist.ConfiggureError(
                "Your config key '%s.port' must be an integer instead of '%s'" %
                (key, self.port))

        try:
            self.fromaddr = the_config['{0}.user'.format(key)]
            self.credentials = (self.fromaddr,
                                the_config['{0}.pasw'.format(key)])
        except AttributeError:
            self.fromaddr = 'atheist@' + socket.gethostname()
            self.credentials = None


        self.ssl = the_config.safe_get('{0}.ssl'.format(key), False)

        if self.credentials is None:
            sender = '<anom>'
        else:
            sender = self.credentials[0]

        self.auto_desc = "SMTP {0} -> {1}:{2}".format(sender, self.host, self.port)


    def run(self):
        self.log.debug("SMTP Connecting: %s:%s" % (self.host, self.port))
        try:
            self.conn = smtplib.SMTP(self.host, self.port)

            if self.ssl:
                self.conn.ehlo()
                self.conn.starttls()
                self.conn.ehlo()

            if self.credentials:
                self.conn.login(*self.credentials)

            return atheist.OK

        except (smtplib.SMTPAuthenticationError, socket.error), e:
            self.log.error(e)
            return atheist.FAIL

    def send(self, toaddrs, subject, data, **kargs):
        return SendMail(self.mng, self, toaddrs, subject, data, **kargs)

    def str_param(self):
        return self.key


class SendMail(atheist.Task):
    acro = 'Mail'

    def __init__(self, mng, smtp, toaddrs, subject, data, **kargs):
        self.smtp = smtp

        if isinstance(toaddrs, str):
            toaddrs = [toaddrs]

        self.toaddrs = toaddrs

        self.subject = subject
        self.data = data

        atheist.Task.__init__(self, mng, **kargs)

        self.misconfig = self.smtp.misconfig
        self.auto_desc = "SMTP '{0}' -> {1}".format(subject, toaddrs)


    def configure(self):
        if not self.smtp.conn:
            raise atheist.ConfigureError("SendMail without SMTP task!!")


    def run(self):
        from email.utils import formatdate

        msg = "From: %s\r\nTo: %s\r\nSubject: %s\r\nDate: %s\r\n\r\n%s" % (
            self.smtp.fromaddr, str.join(',', self.toaddrs),
            self.subject, formatdate(), self.data)

        try:
            self.smtp.conn.sendmail(self.smtp.fromaddr, self.toaddrs,
                                    msg.encode('utf-8'))
            return atheist.OK

        except (smtplib.SMTPSenderRefused, smtplib.SMTPDataError), e:
            self.log.error(e)
            return atheist.FAIL

        finally:
            self.smtp.close()
