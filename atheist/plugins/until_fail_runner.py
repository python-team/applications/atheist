# -*- mode:python; coding:utf-8 -*-
import time
import optparse
from multiprocessing import Process, Queue

import atheist


class UntilFailRunner(atheist.Runner, atheist.Plugin):

    @classmethod
    def get_options(cls, config, parser):
        return [optparse.Option('-u', '--until-fail', action='store_true',
                                help='Repeat tests until something fails')]

    @classmethod
    def config(cls, config, parser):
        if not config.until_fail:
            return

        if config.keep_going:
            parser.error("-u/--until-fail and -k/--keep-going are incompatible options")

        config.RunnerClass = cls


    def subprocess(self):
        def simple_runner(queue):
            child_runner = atheist.Runner(self.mng)
            results = child_runner.run_suite()
            if results.failures:
                child_runner.report()

            queue.put(results.failures)

        queue = Queue()
        p = Process(target=simple_runner, args=[queue])
        p.start()
        retval = queue.get()
        p.join()
        return retval


    def run(self):
        self.setup()

        i = 1
        while 1:
            try:
                fails = self.subprocess()
            except IOError:
                return 1

            if fails or self.mng.aborted:
                break

            minutes = (time.time() - self.mng.cfg.init_time) / 60
            print "ALL OK, repeat until failure: {0} x {1} tasks in {2:.1f} minutes".format(
                i, self.mng.suite.num_tasks, minutes)

            i += 1
            time.sleep(0.5)

        return fails
