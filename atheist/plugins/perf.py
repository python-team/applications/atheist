# -*- mode:python; coding:utf-8 -*-

import time

import atheist

class TimeLessThan(atheist.Condition, atheist.Plugin):
    """ Check the execution time is less than the specified (in seconds) """

    def __init__(self, t):
        atheist.check_type(t, int)
        self.max = t
        atheist.Condition.__init__(self)
        self.tini = None
        self.tend = None

    def pre_task_run(self, task, condlist):
        self.tini = time.time()
        return atheist.Condition.pre_task_run(self, task, condlist)

    def run(self):
        self.tend = time.time()

        assert self.tini is not None, "Internal error"
        assert self.tend is not None, "Internal error"

        return self.tend - self.tini <= self.max

    def __eq__(self, other):
        return atheist.Condition.__eq__(self, other) and \
            self.max == other.max
