# -*- mode:python; coding:utf-8 -*-

import sys
import time
import optparse
from multiprocessing import Process

import pyinotify as inotify

from atheist import Runner, Plugin
from atheist.utils import relpath
from atheist.gvar import Log

class INotifyRunner(Runner, Plugin):
    '''Re-run when a monitor file changes'''

    path = None
    watch_tests = False

    def __init__(self, mng):
        Runner.__init__(self, mng)
        self.tinit = time.time()


    @classmethod
    def get_options(cls, config, parser):
        return [optparse.Option("--watch-tests", action='store_true',
                                help='Automatically re-run tests when they change'),
                optparse.Option("--watch-file",
                                help='Automatically re-run tests when the given file or directory changes')
                ]

    @classmethod
    def config(cls, config, parser):
        if config.watch_tests:
            cls.watch_tests = True
            config.RunnerClass = cls

        if config.watch_file:
            cls.path = config.watch_file
            config.RunnerClass = cls


    def subprocess(self):
        def simple_runner():
            Runner(self.mng).run()

        p = Process(target=simple_runner)
        p.start()
        time.sleep(1)
        p.join()


    def run(self):
        class EventHandler(inotify.ProcessEvent):
            def __init__(self, runner, watch_mng, paths, flags):
                self.runner = runner
                self.wm = watch_mng
                self.paths = paths
                self.flags = flags
                self.wd = {}

                self.on()


            def on(self):
                self.wd = self.wm.add_watch(self.paths, self.flags, rec=True, do_glob=True)


            def off(self):
                self.wm.rm_watch(self.wd.values())
                self.wd.clear()


            def process_default(self, event):
                if event.mask & self.flags == 0:
                    return

#                self.off()
                Log.info("%s: %s", event.maskname, relpath(event.pathname))
                self.runner.subprocess()
#                self.on()


        def stop():
            Log.info('stopping notifier')
            self.notifier.stop()
            self.notifier.stop()


        self.setup()
        self.subprocess()

        paths = []
        if self.watch_tests:
            print "Waiting for changes in test files...",
            sys.stdout.flush()
            paths.extend([case.fname for case in self.suite])

        if self.path:
            print "Waiting for changes in '%s'" % self.path
            paths.append(self.path)


        wm = inotify.WatchManager()
        handler = EventHandler(self, wm, paths, inotify.IN_MODIFY)
        self.notifier = inotify.Notifier(wm, handler, read_freq=1, timeout=1)

        self.mng.abort_observers.append(stop)
        try:
            self.notifier.loop()
            return
        except KeyError:
            pass
