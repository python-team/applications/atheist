# -*- coding:utf-8 -*-

import sys
import doctest
import atheist

class DocTest(atheist.Plugin, atheist.Task):
    acro = 'DocT'
    allows = ['cwd', 'must_fail']
    concurrent = False

    def __init__(self, mng, module_name, **kargs):
        self.module_name = module_name
        self.check = True

        atheist.Task.__init__(self, mng, **kargs)
        self.wrap_outs = True


    def run(self):
        self.log.debug("runing doctest")

        if self.cwd:
            sys.path.append(self.cwd)

        try:
            module = __import__(self.module_name, fromlist=self.module_name.split('.')[-1])
            self.log.debug("%s" % module)
            retval = (doctest.testmod(module)[0] == 0)
            del module
            return retval

        except ImportError, e:
            atheist.Log.warning(e)
            return atheist.ERROR


    def str_param(self):
        return self.module_name
