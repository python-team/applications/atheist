# -*- mode: python; coding: utf-8 -*-

import logging
Log = logging.getLogger('atheist')

import atheist
import os
import commands

class DebPkgInstalled(atheist.Condition, atheist.Plugin):
    """Check the given debian package is installed. Optionally you
    may ask for a minimum version"""

    def __init__(self, package, min_version=None):
        assert isinstance(package, str)
        self.package = package
        self.version = min_version
        atheist.Condition.__init__(self)

    def run(self):
        rcode,info = atheist.run_cmd("dpkg -l %s | grep ^ii" % self.package)
        retval = (rcode == 0)

        if retval and self.version:
            installed = info.split()[2].strip()
            retval &= installed >= self.version

            Log.debug("%s: pkg:%s inst:%s req:%s" %
                       (self.__class__.__name__, self.package,
                        installed, self.version))
        return retval

    def __eq__(self, other):
        return atheist.Condition.__eq__(self, other) \
            and self.package == other.package \
            and self.version == other.version

    def basic_info(self):
        return u"'%s'" % self.package



class DebPkgBuild(atheist.Subprocess, atheist.Plugin):
    """Test a Debian package building from 'debian' directory by using pbuilder"""

    def __init__(self, debian_dir, timeout=3600):
        assert isinstance(debian_dir, str)
        buildir = '/tmp/'
        atheist.Subprocess.__init__(self, "pdebuild --buildresult %s" % buildir, timeout=timeout)

        self.cwd = debian_dir
        self.pre += DebPkgInstalled("pbuilder")

        fchlog   = file(os.path.abspath(self.cwd + "/debian/changelog"), 'r')
        version  = fchlog.readline().split()[1].strip('()')
        fchlog.close()

        fcontrol = file(os.path.abspath(self.cwd + "/debian/control"), 'r')

        self.__pkgs = []

        for l in fcontrol.read().split('\n\n'):
            try:
                name, arch = l.splitlines()[0:2]
                if "Package:" not in name:
                    continue

                name = name.split(':')[1].strip()
                arch = arch.split(':')[1].strip()

                if arch == "any":
                    arch = commands.get_output('dpkg-architecture -qDEB_BUILD_ARCH')

                self.__pkgs.append(os.path.join(buildir, name + '_' + version + '_' + arch + ".deb"))

            except IndexError,e:
                continue

        fcontrol.close()

        if not self.__pkgs:
            Log.error("No packages declared at '%s/debian/control' file" % self.cwd)
            sys.exit(1)

        for p in self.__pkgs:
            self.gen += p

    @property
    def packages(self):
        return self.__pkgs

    def str_param(self):
        return os.path.abspath(self.cwd + "/debian")


class DebPkgInstall(atheist.Subprocess, atheist.Plugin):
    """Test a Debian package installation using piuparts"""

    def __init__(self, deb_files,
                 timeout = 3600,
                 mirrors = ["http://ftp.debian.de/debian"],
                 basetgz = None,
                 verify_signs = True,
                 warn_broken_symlinks = False):

        assert isinstance(deb_files, list)
        assert all([isinstance(x, str) for x in deb_files])

        self.debs = deb_files

        cmd = "sudo piuparts "

        if not self.debs:
            Log.error("No packages to install")
            sys.exit(1)

        for d in self.debs:
            cmd += '%s ' % d
        for m in mirrors:
            cmd += '-m %s ' % m

        if basetgz:
            assert isinstance (basetgz, str)
            cmd += '-b "%s" ' % basetgz

        if not verify_signs:
            cmd += '--do-not-verify-signatures 1 '

        if warn_broken_symlinks:
            cmd += '-W '

        atheist.Subprocess.__init__(self, cmd, timeout=timeout)

        self.pre += DebPkgInstalled("piuparts")
        for d in self.debs:
            self.pre += atheist.FileExists(d)

    def str_param(self):
        return str(self.debs)
