# -*- mode: python; coding: utf-8 -*-

# Copyright (C) 2009 David Villa Alises
#
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import signal, time
import sys
import atexit

abort = False

def abort_handler(signal, frame):
    global abort
    print "C-c pulsado"
    sys.stdout.flush()
    abort = True

signal.signal(signal.SIGINT, abort_handler)
#atexit.register(abort_handler, 0,0)
while 1:
    time.sleep(1)
    if abort:
        print 'quit'
        break

sys.exit(3)
