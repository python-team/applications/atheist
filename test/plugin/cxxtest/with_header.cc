// MyTestSuite.h
#include <cxxtest/TestSuite.h>
#include "add.h"

int add(int a, int b) {
	return a+b;
}

class MyTestSuite : public CxxTest::TestSuite
{
public:
   void testAddition(void)
   {
		 TS_ASSERT(add(1,1) == 2);
   }

};
