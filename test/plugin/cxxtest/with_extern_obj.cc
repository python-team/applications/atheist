// MyTestSuite.h
#include <cxxtest/TestSuite.h>
#include <cxxtest/add.h>

class MyTestSuite : public CxxTest::TestSuite
{
public:
   void testAddition(void)
   {
		 TS_ASSERT(add(1,1) == 2);
   }

};
