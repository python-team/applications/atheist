#!/usr/bin/python
# -*- mode:python; coding:utf-8; tab-width:4 -*-

import sys
import os
import unittest

from mock import Mock

sys.path.extend(['.', '$basedir/atheist'])
from plugins.cxxtest import CxxTest

import atheist
import atheist_mock
from atheist.plugins.debianpkg import DebPkgInstalled


class TestCxxTest(unittest.TestCase):
    def setUp(self):
        self.mng = atheist_mock.MockManager()

    def test_creation_with_valid_single_file(self):
        cut = CxxTest(self.mng, 'test/cxxtest/single_ok.cc')
        self.assertEquals(cut.acro, 'Cxx')
        self.assertEquals(cut.testfile, 'test/cxxtest/single_ok.cc')

    def test_creation_with_invalid_single_file(self):
        fname = 'test/cxxtest/NOEXIST.cpp'
        t = CxxTest(self.mng, fname)
        self.assert_(atheist.FileExists(fname) in t.children[0].pre)

    def test_creation_without_compilation_flags(self):
        cut = CxxTest(self.mng, 'test/cxxtest/single_ok.cc')
        self.assertEquals(cut.compiling_flags, '-lcxxtest')

    def test_creation_with_others_compilation_flags(self):
        cut = CxxTest(self.mng, 'test/cxxtest/single_ok.cc', compiling_flags='-I/tmp')
        self.assertEquals(cut.compiling_flags, '-I/tmp -lcxxtest')

    def test_creation_with_objs(self):
        cut = CxxTest(self.mng, 'test/cxxtest/single_ok.cc', objs={'$testdir':['add.o','diff.o']})
        self.assertEquals(cut.objs, {'$testdir':['add.o','diff.o']})

    def test_get_information_of_params(self):
        cut = CxxTest(self.mng, 'test/cxxtest/single_ok.cc')
        self.assertEquals(cut.str_param(), 'test/cxxtest/single_ok.cc')

    def test_cxxtest_children(self):
        cut = CxxTest(self.mng, 'test/cxxtest/single_ok.cc')

        children = [CxxTest.Generator, CxxTest.CxxCompiler, atheist.Subprocess]
        for i,klass in enumerate(children):
            self.assertTrue(isinstance(cut.children[i], klass))

    def test_generator_child(self):
        full_filename = os.path.abspath('test/cxxtest/single_ok.cc')
        runner = os.path.join(atheist.ATHEIST_TMP, 'single_ok_runner.cc')

        cxx = CxxTest(self.mng, full_filename)
        cut = cxx.children[0]

        self.assert_(runner in cut.gen)

        self.assertEquals(cut.cmd,
                          'cxxtestgen --error-printer -o {0} {1}'.format(
                runner, full_filename))

        self.assertEquals(full_filename, cut.str_param())

    def test_cxxcompiler_child(self):
        cxx = CxxTest(self.mng, 'test/cxxtest/single_ok.cc')
        runner = os.path.join(atheist.ATHEIST_TMP, 'single_ok_runner')

        cut = cxx.children[1]

        self.assert_(runner in  cut.gen)
        self.assertEquals(runner+'.cc', cut.str_param())

    def test_cxxcompiler_child_with_flags(self):
        cxx = CxxTest(self.mng, 'test/cxxtest/single_ok.cc', compiling_flags='-I/tmp')
        cut = cxx.children[1]

        flags = '-I/tmp -lcxxtest'
        input_file = os.path.join(atheist.ATHEIST_TMP, 'single_ok_runner.cc')
        output_file = input_file[:-3]

        self.assertEquals(cut.flags, flags)
        self.assertEquals(cut.objs,[])
        self.assertEquals(cut.outputfile, output_file)
        self.assertEquals(cut.cmd, 'g++ %s %s  -o %s' % (input_file, flags, output_file))


    def test_cxxcompiler_child_with_objs(self):
        cxx = CxxTest(self.mng, 'test/cxxtest/single_ok.cc',objs={'.':['add.o','diff.o']})
        cut = cxx.children[1]

        flags = '-lcxxtest'
        objs = ['./'+x for x in ['add.o','diff.o']]
        input_file =  os.path.join(atheist.ATHEIST_TMP, 'single_ok_runner.cc')
        output_file = input_file[:-3]

        self.assertEquals(cut.flags, flags)
        self.assertEquals(cut.objs,objs)
        self.assertEquals(cut.outputfile,output_file)
        self.assertEquals(cut.cmd, 'g++ %s %s %s -o %s' % (input_file,
                                                           flags,
                                                           str.join(' ', objs),
                                                           output_file))


UnitTestCase(TestCxxTest)
