# -*- mode:python; coding:utf-8 -*-

import logging
import unittest
from optparse import Option, OptionParser

from mock import Mock

import atheist

logging.info("using %s", atheist.__file__)

from atheist.plugins.alias import Alias


class TestAlias(unittest.TestCase):

    def parser_with_options(self):
        parser = OptionParser()
        parser.add_option("--plain", action='store_true', default=False)
        parser.add_option("--disable-bar", action='store_true', default=False)
        parser.add_option("--cols", type=int)
        return parser

    def config_with_alias(self):
        config = Mock()
        config.alias.items._return_value = [('hudson', '--plain --cols 120 --disable-bar')]
        config.plain = False
        config.disable_bar = False
        config.cols = 80
        return config

    def test_add_options_includes_alias_in_group(self):
        config = self.config_with_alias()
        parser = self.parser_with_options()

        group = Alias.get_options(config, parser)

        self.assert_(len(group) == 1)
        opt = group[0]
        self.assert_(opt.get_opt_string() == '--hudson')
        self.assert_(opt.action == 'store_true')


    def test_add_options_with_no_alias_section_does_nothing(self):
        class Empty: pass

        config = Empty()
        parser = Mock()

        group = Alias.get_options(config, parser)

        self.assert_(len(group) == 0)


    def test_config_apply_alias_on_cmdline(self):

        config = self.config_with_alias()
        config.hudson = True

        parser = self.parser_with_options()

        Alias.config(config, parser)

        self.assert_(config.plain)
        self.assert_(config.disable_bar)
        self.assert_(config.cols == 120)


    def test_config_do_NOT_apply_when_no_alias_on_cmdline(self):
        config = self.config_with_alias()
        config.hudson = False

        parser = self.parser_with_options()

        Alias.config(config, parser)

        self.assert_(config.plain == False)
        self.assert_(config.disable_bar == False)
        self.assert_(config.cols == 80)



UnitTestCase(TestAlias)
