#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

import ConfigParser

class IniParser(ConfigParser.SafeConfigParser):

    default_section = 'DEFAULT'

    class Section:
        def __init__(self, parser, section_name):
            self.parser = parser
            self.section_name = section_name

        def keys(self):
            return [k for k,v in self.items()]

        def items(self):
            return self.parser.items(self.section_name)

        def __getattr__(self, key):
            try:
                return self.parser.get(self.section_name, key)
            except ConfigParser.NoOptionError:
                raise AttributeError(key)

        def __str__(self):
            return "<Section '%s:%s'>" % (self.parser.fname, self.section_name)



    def __init__(self, fname=None):
        self.fname = fname
        ConfigParser.SafeConfigParser.__init__(self)
        if fname:
            self.read(fname)

    def keys(self):
        return self.Section(self, self.default_section).keys()


    def __getitem__(self, path):
        if '.' in path:
            section, key = path.split('.')
        else:
            section, key = self.default_section, path

        return getattr(self.Section(self, section), key)


    def __getattr__(self, section):
        if section.startswith('__'):
            raise AttributeError(section)

        if not self.has_section(section):
            raise ConfigParser.NoSectionError(section)

        return self.Section(self, section)


    def safe_get(self, path, default=None):
        try:
            return self[path]
        except AttributeError,e:
            if default is not None:
                return default
            raise
