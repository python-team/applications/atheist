#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

import os

class ActualFileSystem:

    def open(self, fname, mode='r'):
        return open(fname, mode)

    def mkdir(self, path):
        os.mkdir(path)
