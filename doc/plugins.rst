Plugins
-------

It is possible to create plugin for Task and Conditions. This section
includes information about off-the-shelf plugins distributed with atheist.


Tasks
^^^^^

.. function:: DebPkgBuild



.. function:: DebPkgInstall



.. function:: DocTest

   Wrapper to add standard `doctest
  <http://docs.python.org/library/doctest.html>`_ to atheist testcases.


.. function:: TaskTerminator(task, [task,...], key-vals)

   It's a special test to kill and **ensure** the termination of other
   tasks.

   The next example runs a netcat server for 10 seconds and then kills it::

     nc = Daemon("nc -l -p 2000")
     TaskTerminator(nc, delay=10)



.. _unittestcase:

.. function:: UnitTestCase

   Wrapper for standard ``unitest.TestCase``. A sample::

     class TestSample(unittest.TestCase):
	     def test_trivial():
		     self.assertEqual(1, 1)

     UnitTestCase(TestSample)


.. function:: WebTest

   It is a test to check webpages are correct and accessible.


   **Authentication with cookie:**

   If you want to get a page from a restricted site you may need a cookie
   file::

     WebTest('example.org', cookie='cookie.txt')

   To get the cookie you may use a ``curl`` command similar to this::

     curl -c cookie.txt -d "name=John.Doe&pass=secret&form_id=user_login" http://example.orrg/login




Conditions
^^^^^^^^^^

.. function:: CompositeCondition(oper, condition, [condition,...])

   Apply an arbitrary operator to compose results for given
   condition. It is the same idea of `compositetask`_ but applied to
   conditions. Sample::

     CompositeCondition(all, cond1, cond2)


.. function:: DebPkgInstalled(package)

   Checks that Debian package *package* is installed.


.. function:: Or(condition, [condition,...])

   Checks that at least one of the given conditions is satisfied.


.. function:: TaskFinished(task, [task,...])

   Checks that all the given tasks are finished. Usually used with a
   ``Poll`` decorator.


.. function:: TaskRunning(task, [task,...])

   Check that all the given tasks are running.



Extending atheist
-----------------

Customizable Task methods:

.. function:: terminate()

   Ask task to finish as soon as possible and free its resources.
   Returns nothing.

.. function:: is_running()

   Returns a boolean about task is already running.


Customizable Conditions methods:

.. function:: __eq__()

   By default, conditions of same class are the same condition. You
   need to overwrite the __eq__() method to diferenciate instances of
   your Condidition subclass, depending on their attributes


.. Local Variables:
..  coding: utf-8
..  mode: flyspell
..  ispell-local-dictionary: "american"
.. End:
